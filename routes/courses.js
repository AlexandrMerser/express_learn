const {Router} = require('express');
const {omit} = require('ramda');
const isAuthMiddleware = require('../middleware/checkAuth');
const Course = require('../models/courses');

const coursesRouter = Router();

coursesRouter.get('/',  isAuthMiddleware, async (req, res) => {
    try {
        const courses = await Course.find({
            userId: req.user._id
        });

        res.render('courses', {
            title: "Курсы",
            isCourses: true,
            courses
        });
    } catch (error) {
        throw error;
    }
});

coursesRouter.get('/add', isAuthMiddleware, (req, res) => {
    res.render('add', {
        title: "Добавить курс",
        isAdd: true
    })
});

coursesRouter.post('/add', isAuthMiddleware, async (req, res) => {
    try {
        const course = new Course({
            title: req.body.title,
            price: req.body.price,
            imageUrl: req.body.imageUrl,
            userId: req.user,
        });

        await course.save();
        res.redirect('/courses');
    } catch (error) {
        console.log(error);
    }
});

coursesRouter.get('/:id', isAuthMiddleware, async (req, res) => {
    try {
        const id = req.params.id;
        const course = await Course.findById(id);
        res.render('', {
            layout: "courseDetail",
            course
        });
    } catch (error) {
        throw error;
    }
});

coursesRouter.get('/:id/edit', isAuthMiddleware, async (req, res) => {
    try {
        const course = await Course.findById(req.params.id);
        res.render('editCourse', {
            title: `Редактировать курс - ${course.title}`,
            course
        })
    } catch (error) {
        throw error;
    }
});

coursesRouter.post('/edit', isAuthMiddleware, async (req, res) => {
    try {
        const {id} = req.body;

        const bodyWithoutId = omit(['id'], req.body);
        await Course.findByIdAndUpdate(id, omit(['id'], bodyWithoutId));
        res.redirect(`/courses/${id}`);
    } catch (error) {
        throw error;
    }
});

coursesRouter.post('/delete', async (req, res) => {
    try {
        await Course.deleteOne({
            _id: req.body.id
        })
        res.redirect(`/courses`);
    } catch (error) {
        throw error;
    }
});

module.exports = coursesRouter;