const {SENDGRID_API_KEY} = require('../keys');
const {mailRegister, resetPassword} = require('../emails');

const {Router} = require('express');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const sendGridTransport = require('nodemailer-sendgrid-transport');
const crypto = require('crypto');
const { validationResult } = require('express-validator');
const {registerValidators} = require('../utils/validators');

const transporter = nodemailer.createTransport(sendGridTransport({
    auth: {
        api_key: SENDGRID_API_KEY
    }
}));

const User = require('../models/user');

const loginRoute = Router();

loginRoute.get('/login', async (req, res) => {
    res.render('login/index', {
        isLogin: true,
        title: 'Авторизация',
        errorLogin: req.flash('errorLogin'),
        errorRegister: req.flash('errorRegister')
    })
});

loginRoute.post('/login', async (req, res) => {
    try {
        const {email, password} = req.body;
        const foundUser = await User.findOne({email});

        if(foundUser) {
            const isSamePassword = await bcrypt.compare(password, foundUser.password);
            if(isSamePassword) {
                req.session.user = foundUser;
                req.session.isAuthenticated = true;
                return req.session.save((error) => {
                    if(error) throw error;
                    res.redirect('/');
                });
            }
            req.flash('errorLogin', 'Неверный пароль');
            return res.redirect('/auth/login#login');
        }
        req.flash('errorLogin', 'Пользователя с таким email не существует');
        return res.redirect('/auth/login#login')
    } catch(error) {
        throw error;
    }
});

loginRoute.post('/register', registerValidators, async (req, res) => {
    try {
        const {name, email, password} = req.body;
        const errors = validationResult(req);

        if(!errors.isEmpty()) {
            req.flash('errorRegister', errors.array()[0].msg);
            return res.status(422).redirect("/auth/login#registration");
        }

        const encryptedPassword = await bcrypt.hash(password, 10);
        const newUser = await new User({
            name,
            email,
            password: encryptedPassword,
            cart: {
                items: []
            }
        })

        await newUser.save();
        res.redirect('/auth/login#login');

        await transporter.sendMail(mailRegister(email))
    }

    catch (error) {
        console.log(error);
    }
});

loginRoute.get('/logout', async (req, res) => {
    req.session.isAuthenticated = false;
    req.session.destroy((error) => {
        if(error) throw error;
        res.redirect('/auth/login#login');
    });
});

loginRoute.get('/reset', (req, res) => {
    res.render('login/reset', {
        title: 'Забыли пароль?',
        error: req.flash('forgetError')
    })
});

loginRoute.post('/reset', (req, res) => {
    try {
        crypto.randomBytes(32, async (error, buffer) =>{
            if(error) {
                req.flash('forgetError', 'Что-то пошло не так, попробуйте позже :(');
                return res.redirect('/auth/reset');
            }
            const token = buffer.toString('hex');
            const candidate = await User.findOne({
                email: req.body.email
            })

            if(candidate){
                candidate.resetToken = token;
                candidate.resetTokenDate = Date.now() + 60 * 60 * 1000;
                await candidate.save();
                await transporter.sendMail(resetPassword(candidate.email, token));
                return res.redirect('/auth/login#login');
            }

            req.flash('forgetError', 'Такого e-mail не существует.');
            res.redirect('/auth/reset');
        });
    } catch (error) {
        console.log(error);
    }
});

loginRoute.get('/password/:token', async (req, res) => {
    try {
        const user = await User.findOne({
            resetToken: req.params.token,
            resetTokenDate: {
                $gt: Date.now()
            }
        });

        if(!user) {
            return res.redirect('/auth/login#login');
        }

        res.render('login/newPassword', {
            title: 'Новый пароль',
            userId: user._id.toString(),
            token: req.params.token
        })
    } catch (error) {
        console.log(error);
    }
});

loginRoute.post('/new-password', async (req, res) => {
    try {
        const user = await User.findOne({
            _id: req.body.userId,
            resetToken: req.body.token,
            resetTokenDate: {
                $gt: Date.now()
            }
        })

        if(!user) {
            return res.redirect('/auth/login#login');
        }

        user.password = await bcrypt.hash(req.body.password, 10);
        user.resetToken = undefined;
        user.resetTokenDate = undefined;
        await user.save();
        res.redirect('/auth/login#login');

    } catch (error) {
        console.log(error);
    }
});

module.exports = loginRoute;