const {Router} = require('express');
const OrderModel = require('../models/orders');
const isAuthMiddleware = require('../middleware/checkAuth');

const {extractCoursesFromUserCart, formatOrdersToClient} = require('../helpers/index');

const ordersRouter = Router();

ordersRouter.get("/", isAuthMiddleware, async (req, res) => {
    try {
        const orders = await OrderModel.find({
            'user.userId': req.user._id
        }).populate('user.userId');

        res.render("orders", {
            title: 'Мои заказы',
            isOrders: true,
            orders: formatOrdersToClient(orders)
        });
    } catch (error) {
        console.log(error);
    }
});

ordersRouter.post("/", isAuthMiddleware, async (req, res) => {
    try {
        const user = await req.user
            .populate('cart.items.courseId')
            .execPopulate();
        const courses = extractCoursesFromUserCart(user);

        const order = new OrderModel({
            user: {
                name: req.user.name,
                userId: req.user
            },
            courses
        });

        await order.save();
        await req.user.clearCart();

        res.redirect('/orders');
    } catch (error) {
        console.log(error);
    }
});

module.exports = ordersRouter;