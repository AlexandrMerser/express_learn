const {Router} = require('express');
const isAuthMiddleware = require('../middleware/checkAuth');
const User = require('../models/user');

const profileRouter = Router();

profileRouter.get('/', isAuthMiddleware, async (req, res) => {
    const user = await req.user;

    res.render('profile', {
        title: 'Профиль',
        isProfile: true,
        user
    })
});

profileRouter.post('/', isAuthMiddleware, async (req, res) => {
    try {
        let user = await User.findById(req.user._id);

        if(req.body.name !== user.name) {
            user.name = req.body.name;
        }
        if(req.file) {
            user.avatarUrl = req.file.path;
        }

        await user.save();
        return res.redirect('/profile');
    } catch (error) {
        console.log(error)
    }
});

module.exports = profileRouter;