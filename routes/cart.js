const {pick} = require('ramda');
const {Router} = require('express');
const Course = require('../models/courses');
const isAuthMiddleware = require('../middleware/checkAuth');

const cartRouter = Router();

const computeTotalPrice = (courses) => {
    return courses.reduce((totalSum, {price, count}) => {
        return totalSum += price * count;
    }, 0);
};
const formatCartData = (cart) => {
    return cart.map(cartItem => {
        return {
            count: cartItem.count,
            _id: cartItem._id,
            courseId: cartItem.courseId._id,
            ...pick(['title', 'price', 'imageUrl', 'userId'], cartItem.courseId)
        }

    })
}


cartRouter.get('/', isAuthMiddleware, async (req, res) => {
    const foundCurrentUser = await req.user;

    if(foundCurrentUser.cart.items.length) {
        try {
            await foundCurrentUser.populate('cart.items.courseId').execPopulate();

            const modifiedCart = formatCartData(foundCurrentUser.cart.items);

            res.render('cart', {
                isCard: true,
                courses: modifiedCart,
                total: computeTotalPrice(modifiedCart)
            });
        } catch (error) {
            throw error;
        }

    } else {
        res.render('cart', {
            isCard: true,
            courses: [],
            total: 0
        });
    }
});

cartRouter.post('/add', isAuthMiddleware, async (req, res) => {
    try {
        const course = await Course.findById(req.body.id);
        await req.user.addToCart(course);
        res.redirect('/cart');
    } catch (error) {
        throw error;
    }
});

cartRouter.delete('/remove/:id', isAuthMiddleware, async (req, res) => {
    await req.user.deleteFromCard(req.params.id);
    res.json({
        _id: req.params.id,
        status: 1
    });
});

module.exports = cartRouter;
