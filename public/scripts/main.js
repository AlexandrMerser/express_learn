const configFormat = {
    style: 'currency',
    currency: "RUB"
}

const configDate = {
    day: '2-digit',
    month: 'long',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit'
};

document.addEventListener('DOMContentLoaded', () => {
    formatValue('.price', 'number', configFormat);
    formatValue('.totalPrice', 'number', configFormat);
    formatValue('.priceCourse', 'number', configFormat);
    formatValue('.date-order', 'date', configDate, (stringDate) => {
        return new Date(stringDate);
    })

    deleteCourseFromCard();

    M.Tabs.init(document.querySelector('.tabs'));
});


function formatValue(selector, type = 'number', config, proxyValueFormat) {
    const checkTypeFormatter = type === 'date' ? 'DateTimeFormat' : 'NumberFormat';

    const prices = document.querySelectorAll(selector);
    const formatter = new Intl[checkTypeFormatter]('ru-RU', config);

    prices.forEach(textNode => {
        textNode.innerHTML = formatter.format(
            proxyValueFormat ? proxyValueFormat(textNode.innerHTML) : textNode.innerHTML
        );
    });
};

function deleteCourseFromCard() {
    const deleteBtns = document.querySelectorAll('.js-remove');

    deleteBtns.forEach(btn => {
        btn.addEventListener('click', (e) => {
            const id = e.target.dataset.id;
            const csrf = e.target.dataset.csrf;

            console.log('id ', id);
            console.log('csrf ', csrf);

            fetch(`/cart/remove/${id}`, {
                method: 'delete',
                headers: {
                    'X-XSRF-TOKEN': csrf
                }
            }).then(res => res.json())
                .then(res => {
                    if(res.status) {
                        location.reload();
                    }
                });
        });
    })
}