module.exports = {
    SENDGRID_API_KEY: process.env.SENDGRID_API_KEY,
    MONGODB_URI: process.env.MONGODB_URI,
    SESSION_SECRET_KEY: process.env.SESSION_SECRET_KEY,
    MAIL_SEND_FROM: process.env.MAIL_SEND_FROM,
    BASE_URL: process.env.BASE_URL,
    PORT: process.env.PORT
};