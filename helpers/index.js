function extractCoursesFromUserCart(user) {
    return user.cart.items.map(item => ({
        count: item.count,
        course: {
            ...item.courseId._doc
        }
    }));
}

function formatOrdersToClient(orders) {
    return orders.map(order => {
        return {
            ...order._doc,
            price: _calculateOrdersSumPrices(order)
        }
    });
}

function _calculateOrdersSumPrices(order) {
    return order.courses.reduce((total, course) => total += course.course.price * course.count, 0);
}

module.exports = {
    extractCoursesFromUserCart,
    formatOrdersToClient
}