const multer = require('multer');

const getNewFileName = (name) => {
    return `${new Date().toISOString()}_${name}`;
}

const storage = multer.diskStorage({
    destination(req, file, callback) {
        callback(null, "images");
    },
    filename(req, file, callback) {
        callback(null, getNewFileName(file.originalname))
    }
});

const allowedTypes = ['image/jpg', 'image/jpeg', 'image/png'];

const fileFilter = (req, file, callback) => {
    if(!allowedTypes.includes(file.mimetype)) {
        return callback(null, false);
    }
    return callback(null, true);
};

module.exports = multer({
    storage,
    fileFilter
});