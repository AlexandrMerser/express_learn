const UserModel = require('../models/user');

module.exports = async (req, res, next) => {
    if(!req.session.user) {
        return next();
    }

    try {
        req.user = await UserModel.findById(req.session.user._id);
        next();
    } catch (error) {
        throw error;
    }
};