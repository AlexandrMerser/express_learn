const {Schema, model} = require('mongoose');

const user = new Schema({
    email: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    avatarUrl: String,
    resetToken: String,
    resetTokenDate: Date,
    password: {
        type: String,
        required: true
    },
    cart: {
        items: [
            {
                count: {
                    type: Number,
                    required: true,
                    default: 1
                },
                courseId: {
                    type: Schema.Types.ObjectId,
                    ref: 'Course',
                    required: true
                }
            }
        ]
    }
});

user.methods.addToCart = function (course) {
    const items = [...this.cart.items];
    const foundCourse = courseItem => courseItem.courseId.toString() === course._id.toString();

    const foundIdxCourse = items.findIndex(foundCourse);

    if(foundIdxCourse >= 0) {
        items[foundIdxCourse].count = items[foundIdxCourse].count + 1;
    } else {
        items.push({
            courseId: course._id,
            count: 1
        })
    }

    this.cart = {
        items
    };

    return this.save();
}


user.methods.deleteFromCard = function(id) {
    const items = [...this.cart.items];

    this.cart = {
        items: items.map(item => {
            if(item._id.toString() === id.toString()) {
                return {
                    ...item._doc,
                    count: item.count - 1,
                }
            }
            return item;
        }).filter(item => item.count > 0)
    }

    return this.save();
};

user.methods.clearCart = function () {
    this.cart = {
        items: []
    }

    return this.save();
}

module.exports = model('User', user);