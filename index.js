const {MONGODB_URI, SESSION_SECRET_KEY, PORT} = require('./keys');

const express = require('express');
const mongoose = require('mongoose');
const exphbs = require('express-handlebars'); // подключаем express-handlebars
const path = require('path');
const helmet = require('helmet');
const compression = require('compression');
const Handlebars = require('handlebars');
const session = require('express-session');
const MongoDbStore = require('connect-mongodb-session')(session);
const varMiddleware = require('./middleware/variables');
const {allowInsecurePrototypeAccess} = require('@handlebars/allow-prototype-access');
const addUserToRequestMiddleWare = require('./middleware/addUserToRequest');
const csrf = require('csurf');
const flash = require('connect-flash');
const handler404 = require('./middleware/handler404');
const fileMulter = require('./middleware/fileMulter');

const store = new MongoDbStore({
   collections: 'sessions',
   uri: MONGODB_URI
});

const homeRoute = require('./routes/home');
const coursesRoute = require('./routes/courses');
const cardRoute = require('./routes/cart');
const orderRoute = require('./routes/orders');
const loginRoute = require('./routes/login');
const profileRoute = require('./routes/profile');

const app = express();

const hbs = exphbs.create({ // конфигурируем handlebars
    defaultLayout: 'main', // указываем имя layout по умолчанию.
    extname: 'hbs', // указываем имя расширения
    handlebars: allowInsecurePrototypeAccess(Handlebars)
});

app.engine('hbs', hbs.engine); // здесь в app (express) в метод engine передаем -  в первый параметр выставляется то самое имя "extname", которое мы указали, вторым параметром передаем сконфигурированный hbs c методом engine - hbs.engine
app.set('view engine', 'hbs'); // - здесь указывается что показывать, то есть по факту вторым параметром указываем тот же extname
app.set('views', 'views'); // - здесь указывается в какую папку смотреть на шаблоны, то есть первый параметр - это папка по умолчанию, а второй - можем изменить на нужную нам папку.

app.use(express.static(path.join(__dirname, 'public'))); //  - здесь указываем папку public как статичную, чтобы экспресс мог ее видеть
app.use('/images', express.static(path.join(__dirname, 'images')));

app.use(express.urlencoded({extended: true}));
app.use(session({
    secret: SESSION_SECRET_KEY,
    resave: false,
    saveUninitialized: false,
    store
}));
app.use(fileMulter.single('avatar'));
app.use(csrf());
app.use(flash());
app.use(compression());
app.use(helmet({
    contentSecurityPolicy: false,
}));
app.use(varMiddleware);
app.use(addUserToRequestMiddleWare);

//Routes
app.use("/", homeRoute);
app.use("/courses", coursesRoute);
app.use("/cart", cardRoute);
app.use("/orders", orderRoute);
app.use("/auth", loginRoute);
app.use("/profile", profileRoute);
app.use(handler404);

// const nameOrganization = 'AlexandrMers'
// const projectName = 'express_learn'
// const dbUserName = 'AlexandrMerser';
// const dbUserPassword = 'xU3skPxAeR';

(async () => {
    try {
        await mongoose.connect(MONGODB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        });

        app.listen(PORT, () => {
            console.log(`server is running on port ${PORT}`);
        });
    } catch (error) {
        console.log(error);
    }
})();