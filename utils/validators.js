const {body} = require("express-validator");
const User = require("../models/user");

const confirmPasswordValidator = body("confirmPassword").custom((value, {req}) => {
    if (value !== req.body.password) {
        throw new Error('Пароли должны совпадать');
    }
    return true
});

const passwordValidator = body("password", "Пароль должен содержать минимум 6 символов, а максимум 56")
    .isLength({
        min: 6,
        max: 56
    })
    .isAlphanumeric()
    .trim();


const emailValidator = body("email")
    .isEmail()
    .custom(async (value, {req}) => {
        try {
            const isExistEmail = await User.findOne({
                email: value
            });

            if (isExistEmail) {
                return Promise.reject("Такой email уже существует")
            }

            return true;
        } catch (error) {
            console.log(error);
        }
    })
    .normalizeEmail();


const nameValidator = body("name", "Имя должно содержать минимум 1 символ").isLength({
    min: 1
});

exports.registerValidators = [
    nameValidator,
    emailValidator,
    passwordValidator,
    confirmPasswordValidator
];