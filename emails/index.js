const {MAIL_SEND_FROM, BASE_URL} = require('../keys');

const mailRegister = (mail) => ({
    to: mail,
    from: MAIL_SEND_FROM,
    subject: 'Аккаунт успешно создан',
    html: `
        <h1>Аккаунт успешно создан, братанчик!</h1>
        <p>Спасибо за регистрацию. Ваш логин - ${mail}</p>
        <a href="${BASE_URL}">Магазин курсов</a>
    `
});

const resetPassword = (mail, token) => ({
    to: mail,
    from: MAIL_SEND_FROM,
    subject: 'Восстановление пароль',
    html: `
        <h1>Вы забыли пароль?</h1>
        <p>Если нет, то проигнорируйте письмо</p>
        <p>Для восстановления пароля перейдите по ссылке ниже</p>
        <a style="background: chartreuse" href="${BASE_URL}/auth/password/${token}">Восстановить пароль</a>
    `
});

module.exports = {
    mailRegister,
    resetPassword
}